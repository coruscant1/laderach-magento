default: base-image

base-image: magento-elasticsearch magento-node

magento-elasticsearch:
	@docker build -t magento-elasticsearch ./docker/magento-elasticsearch

magento-elasticsearch-aws:
	@docker build -t magento-elasticsearch-aws ./docker/magento-elasticsearch-aws
	@docker tag magento-elasticsearch-aws 005636478961.dkr.ecr.eu-central-1.amazonaws.com/laderach/testing/magento-elasticsearch-aws

magento-node:
	@docker build -t magento-node ./docker/magento-node

smileshop:
	@docker build -t smileshop ./docker/smileshop
	@docker tag smileshop 005636478961.dkr.ecr.eu-central-1.amazonaws.com/laderach/testing/magento
